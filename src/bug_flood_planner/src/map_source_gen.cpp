#include <ros/ros.h>
#include <bug_flood/source_goal.h>
#include <nav_msgs/OccupancyGrid.h>

ros::Publisher map_pub;
ros::Publisher source_pub;
bug_flood::source_goal goal_msg;
nav_msgs::OccupancyGrid map_msg;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "talker");
	ros::NodeHandle n;
	goal_msg.source_x=0;
	goal_msg.source_y=0;
	goal_msg.goal_x=0;
	goal_msg.goal_x=0;
	goal_msg.header.stamp=ros::Time::now();
	
	source_pub=n.advertise<bug_flood::source_goal>("/local_goal",1);
	while(ros::ok())
	{
		source_pub.publish(goal_msg);
	}
	return 0;
}