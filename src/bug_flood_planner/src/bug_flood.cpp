#include <ros/ros.h>
#include <bug_flood/bug.h>
#include <bug_flood/bug_flood.h>
#include<bug_flood/traj.h>
#include<cstdint>

using namespace std;
using namespace message_filters;


int g_argc;
char **g_argv;

node::node(ros::NodeHandle* nodehandle):nh_(*nodehandle)
	{
		ROS_INFO("in class constructor of node");
		initializeSubscribers();
		initializePublishers();
		
	}

void node::initializeSubscribers()
	{
		ROS_INFO("Initializing Subscribers");
		map_sub_.subscribe(nh_,"/map",10);
		goal_sub_.subscribe(nh_,"/local_goal",10);
		sync_.reset(new Sync(MySyncPolicy(10), map_sub_, goal_sub_));
		sync_->registerCallback(boost::bind(&node::node_callback,this, _1, _2));
	}

void node::initializePublishers()
{
		ROS_INFO("Initializing Publishers");
		pathPublisher=nh_.advertise<bug_flood::traj>("/bug_flood_path",1);

}
	
	

void node::node_callback(const nav_msgs::OccupancyGrid::ConstPtr& map, const bug_flood::source_goal::ConstPtr& goal)
{
	int width = map->info.width;
	int height = map->info.height;
	int matrix_size = width*height; 
	std::vector<bool> occupancy_grid;
	uint16_t source_goal[4];
	ROS_INFO("Node callback activate");

	for (auto i: map->data)
	{
		if (i==50 || i==0)
		{
			occupancy_grid.push_back(false);
		}
		else
		{
			occupancy_grid.push_back(true);
		}
		
	}

	source_goal[0] = goal->source_x;
	source_goal[1] = goal->source_y;
	source_goal[2] = goal->goal_x;
	source_goal[3] = goal->goal_y;
	final_trajectory.points= bug_flood_traj(occupancy_grid,source_goal,width,height);
	final_trajectory.header.stamp=ros::Time::now();
	pathPublisher.publish(final_trajectory) ;
	for (auto i : final_trajectory.points)
	{
		ROS_INFO("X: %f Y: %f",i.x,i.y);
	}



}


vector<Point> node::bug_flood_traj(std::vector<bool> &v, uint16_t array[], int width,int height)
{
	Environment environment(array,width,height,v);

	Bug finalBug;
	finalBug.setCost(INT_MAX); //this bug will be used to store the min path's bug

	vector<Bug> bugList;
	//add the first bug to the list
	bugList.push_back(Bug(environment.getSource()));


	//start timer
	ros::Time startTime = ros::Time::now();

	while (!bugList.empty())
	{
		//Kill bugs that are done
		KillBugs(bugList, finalBug);

		for (auto i: bugList)
		{
			if (i.getState() == Bug::TOWARDS_GOAL)
			{
				i.TowardsGoal(environment, bugList);
				continue; //to avoid running a bug twice when changing transition through TOWARDS_GOAL to BOUNDARY_FOLLOW
			}
			if(i.getState() == Bug::BOUNDARY_FOLLOWING)
			{
				i.BoundaryFollow(environment);
			}
		}
	
	}
		//print the algo's running time
	ros::Time endTime = ros::Time::now();
	int nsec = endTime.nsec - startTime.nsec;
	int sec = endTime.sec - startTime.sec;
	if(nsec < 0)
	{
		sec -= 1;
		nsec += 1000000000;
	}


	double mainTime = sec + (nsec / 1000000000.0);
	double pruneTime = 0;

	double before_pruning_cost = finalBug.getCost();

	// Before pruning dump the un-pruned path so that it can be used in OMPL PathSimplifier
	string sgPath(g_argv[1]);
	std::size_t found =sgPath.find_last_of("/\\");
	string directoryPath = sgPath.substr(0,found);
	finalBug.DumpPath(directoryPath + "/path" +  sgPath.substr(found+3));
	//path pruning is on by default
	int isPruneON = 1;

	//do path pruning only if pruning is on
	if(isPruneON > 0)
	{
		startTime = ros::Time::now();
		vector<Point> resultingPath = finalBug.getpath();
		double cost;

		if(isPruneON == 1)
			prunePathFirst(resultingPath, environment, cost);
		else
			prunePathLast(resultingPath, environment, cost);



		finalBug.setCost(cost);
		finalBug.setPath(resultingPath);
		endTime = ros::Time::now();
		nsec = endTime.nsec - startTime.nsec;
		sec = endTime.sec - startTime.sec;
		if (nsec < 0) {
			sec -= 1;
			nsec += 1000000000;
		}
	//	ROS_INFO("Pruning Time = %d, %d", sec, nsec);
		pruneTime = sec + (nsec / 1000000000.0);
	}

		ROS_INFO("BUG COST %f", finalBug.getCost());

	//if path is to be published
	// publish_bug(finalBug, pathPublisher, pathTree);

	//logfile
	ofstream logFile;
	logFile.open(g_argv[1],ofstream::app);

	logFile << before_pruning_cost << " " << mainTime << " " << finalBug.getCost() << " " << pruneTime << endl;

	logFile.close();

	//dump final path in a fixed trajectory file
	finalBug.DumpPath("/home/codecraft94/swarath_local/swarath/trajectory_bug_flood.txt");
	ROS_INFO("Main time : %f ms " , mainTime*1000);
	ROS_INFO("Prune time : %f ms ", pruneTime*1000);
	ROS_INFO("----BUG FLOOD TRAJECTORY----");
	return finalBug.getpath();

}	




int main(int argCount, char ** argValues)
{
	//check if proper inputs are passed
	if(argCount < 2)// Check the value of passedArgumentCount. if filename is not passed
	{
		std::cout << "usage -> rosrun bug_flood bug_flood <path to log file>\n";
		// Inform the user on how to use the program
		exit(0);
	}
	g_argc=argCount;
	g_argv=argValues;
		//initializing ROS
	ros::init(argCount,argValues,"bug_flood");
	ros::NodeHandle nh;

	ROS_INFO("main: instantiating an object of type node");
	node bug_flood_node(&nh);
	ROS_INFO("main: going into spin; let the callbacks do all the work");
	
	while(ros::ok())
	{
	ros::spin();	
	}
	
	return 1;
}
