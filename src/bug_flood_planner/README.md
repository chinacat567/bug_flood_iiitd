# bug_flood
A bug based online path planner.

### Input
We take 2 inputs, the occupancy grid (map) and source goal. The map is received on "/map" (nav_msgs/OccupancyGrid) topic and source goal coordinates on "/local_goal" (custom msg). The final trajectory is published on the topic "/bug_flood_path" (custom msg).

#### Bug Flood
usage -> rosrun bug_flood bug_flood <path to log file>
