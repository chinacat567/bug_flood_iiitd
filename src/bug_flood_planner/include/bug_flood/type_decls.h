//
// Created by sumantra on 6/6/16.
//

#ifndef BUG_FLOOD_TYPE_DECLS_H
#define BUG_FLOOD_TYPE_DECLS_H

#include <vector>
#include <geometry_msgs/Point.h>
#include <map>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include<nav_msgs/OccupancyGrid.h>
#include<bug_flood/source_goal.h>

using namespace std;

typedef geometry_msgs::Point Point;

struct Line
{
	Point start;
	Point end;
};

//struct ObstacleLine
//{
//	int boundaryID;
//	Line line;
//};

//typedef vector<ObstacleLine> Obstacle;
typedef map<int, Line> ObstacleLines;
typedef message_filters::sync_policies::ExactTime<nav_msgs::OccupancyGrid,bug_flood::source_goal> MySyncPolicy;
typedef message_filters::Synchronizer<MySyncPolicy> Sync;
//typedef vector<Obstacle> ObstacleList;
typedef vector<Point> Path;

struct VisitInfo
{
	Point location;
	double cost;
};

#endif //BUG_FLOOD_TYPE_DECLS_H
