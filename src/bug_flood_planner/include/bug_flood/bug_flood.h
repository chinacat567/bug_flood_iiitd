//
// Created by sumantra on 15/5/16.
//

#ifndef BUG_FLOOD_BUG_FLOOD_H
#define BUG_FLOOD_BUG_FLOOD_H

#include <ros/ros.h>
#include <vector>
#include <visualization_msgs/Marker.h>
#include <bug_flood/bug.h>
#include <bug_flood/type_decls.h>
#include <bug_flood/traj.h>
#include <message_filters/subscriber.h>
#include<nav_msgs/OccupancyGrid.h>
#include<bug_flood/source_goal.h>

typedef visualization_msgs::Marker Marker;

using namespace std;

class node
{
public:
	node(ros::NodeHandle* nodehandle);
private:
	ros::NodeHandle nh_;
	message_filters::Subscriber<nav_msgs::OccupancyGrid> map_sub_;
	message_filters::Subscriber<bug_flood::source_goal> goal_sub_;
	boost::shared_ptr<Sync> sync_;
	ros::Publisher pathPublisher;
	bug_flood::traj final_trajectory;
	void initializeSubscribers(); 
	void initializePublishers();
	void node_callback(const nav_msgs::OccupancyGrid::ConstPtr& map, const bug_flood::source_goal::ConstPtr& goal);
	vector<Point> bug_flood_traj(std::vector<bool> &v, uint16_t array[], int width,int height);
	
};


void initMarkers(Marker &bugs, Marker &pathTree);
// void publish_bugs(vector<Bug> &bugList, ros::Publisher &publisher, Marker &bugs, Marker &pathTree);
void publish_bug(Bug &bug, ros::Publisher &publisher, Marker &pathTree);
void publish_bug_final(Bug &bug, ros::Publisher &publisher, bug_flood::traj &pathTree);


//#define KINEMATIC_BUG

#ifdef KINEMATIC_BUG

#define RABBIT_STEP_SIZE 0.5

bool updateCarrotWhenMovingOnLine(Line line, Point &rabbit, Point &carrot);
void updateRabbit(Point &rabbit, Point carrot);
void pruneRabbitCarrot(vector<Point> &inPath, double &cost);
#endif

#endif //BUG_FLOOD_BUG_FLOOD_H
